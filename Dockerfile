FROM python:3.8-slim-buster
COPY main.py /app/main.py
WORKDIR /app
COPY  requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
RUN python -m spacy download en_core_web_sm



