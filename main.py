"""

Run the modules necessary to import SO questions and stores them as artifact after running a Pipeline.

Author: William Arias

v 1.0

"""


from LoaderEngine.extract import Extract
from transform import transform_json
from store import writes_json
import logging
import os

logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


def main():
    """
    This function:

        1. Pulls data from Stackoverflow API
        2. Takes JSON response from API and cleans it and transform it to a new JSON
           with relevant fields
        3. Writes transformed response and is made available as Pipeline Artifact
        4. Converts JSON to CSV

    """

    col_ep_questions = 'https://api.stackoverflowteams.com/2.3/questions'

    payload = {'page': 1, 'pagesize': 100,
               'order': 'desc', 'sort': 'creation', 'team': 'gitlab-customer-success', 'X-API-Access-Token': os.environ.get('STACKOVERFLOW_API')}

    endpoint_caller = Extract(col_ep_questions, payload)
    logger.info("INFO: API Call - Calling SO endpoint")

    # 1.
    questions = endpoint_caller.load_questions()
    logger.info(questions)
    # Return JSON encoded content from response - Requests object
    questions = questions.json()

    # 2.
    filter_questions = transform_json(questions)
    # 3.
    writes_json(filter_questions)
    logger.info("SUCCESS: API Call - Generated JSON response made available as artifact")

    # 4.


if __name__ == "__main__":
    main()
