import requests
import os

BASE_URL = 'api.stackoverflowteams.com/2.3'
COLLECTIVE_ENDPOINT_QUESTIONS = 'https://api.stackoverflowteams.com/2.3/questions'

payload = {'X-API-Access-Token': os.environ.get(STACKOVERFLOW_API), 'page': 1, 'pagesize': 1, 'order': 'desc', 'sort': 'creation', 'team': 'gitlab-customer-success'}
questions = requests.get(COLLECTIVE_ENDPOINT_QUESTIONS, params=payload)

print(questions.url)
print(questions.json()['items'][0]['title'])
print(f"question asked was:\n{questions.json()['items'][0]['title']}")
