"""

Imports and loads SO questions that contain 'gitlab' label.

Author: William Arias

v 1.0

"""

import requests


class Extract:
    """
    A loader object encapsulates the results from querying SO API with the fields required to be written
    in the database

    """

    def __init__(self, endpoint, payload):
        self.endpoint = endpoint
        self.payload = payload

    def load_questions(self):
        questions = requests.get(self.endpoint, params=self.payload)
        return questions


