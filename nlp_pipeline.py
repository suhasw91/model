"""

Run NLP Pipeline and outputs CSV file.

Author: William Arias

v 1.0

"""

from pathlib import Path
import pandas as pd
import spacy
import string
import ast
import logging



logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


dataset_path = "questions/dataset-questions.json"

nlp = spacy.load("en_core_web_sm")


def create_df(dataset):
    questions_df = pd.read_json(dataset)
    return questions_df


def preprocess_time(dataset):
    dataset['creation_date'] = pd.to_datime(dataset['creation_date'], unit='s')
    return dataset


def lowercase_columns(dataset):
    dataset['title_prc'] = dataset['title'].str.lower()
    dataset['body_prc'] = dataset['body'].str.lower()
    return dataset


def remove_stop_words(text):
    nlp.Defaults.stop_words.add("gitlab")
    all_stopwords = nlp.Defaults.stop_words
    tokens = text.split(" ")
    corpus_nsw = [word for word in tokens if not word in all_stopwords]
    return " ".join(word for word in corpus_nsw)


def lemmatize(text):
    lemmatizer = nlp.get_pipe("lemmatizer")
    document = nlp(text)
    lemma_list = [token.lemma_ for token in document]
    return lemma_list


def remove_punctuation(text):
    """Gets the content of a cell in the dataframe, and remove punctuation signs
        This function used translate(maketrans(from,to, del))

        Parameters
        ----------
        sentence : str
            Sentence or text within the cell in the dataframe in a list


        Returns
        -------
        sentence: str
            sentence without punctuation
        """

    punct = string.punctuation
    return text.translate(text.maketrans("", "", punct))


def n_grams(tokens, n=1):
    ngrams = [tokens[i:i+n] for i in range(len(tokens)-n+1)]
    ngrams_txt = [" ".join(ngram) for ngram in ngrams]
    return ngrams_txt


def flatten(a_list):
    return [item for sublist in a_list for item in sublist if item != ' ' and item != '']


def pd_csv(path, dataframe):
    return dataframe.to_csv(path)


df = create_df(dataset_path)

df = lowercase_columns(df)

df['title_prc'] = df['title_prc'].apply(remove_stop_words)
df['body_prc'] = df['body_prc'].apply(remove_stop_words)

df['title_prc'] = df['title_prc'].apply(remove_punctuation)
df['body_prc'] = df['body_prc'].apply(remove_punctuation)

df['title_prc'] = df['title_prc'].apply(lemmatize)
df['body_prc'] = df['body_prc'].apply(lemmatize)


df['title_prc_bigram'] = df['title_prc'].apply(n_grams, n=2)
df['body_prc_bigram'] = df['body_prc'].apply(n_grams, n=2)
df['title_prc_trigram'] = df['title_prc'].apply(n_grams, n=3)
df['body_prc_trigram'] = df['body_prc'].apply(n_grams, n=3)


filepath = Path('questions/dataset-questions.csv')
data_csv = pd_csv(filepath, df)
logger.info("SUCCESS: csv file generated")
