# Tests the loader didn't store profile image URL from API Call


import logging
import pandas as pd

logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


DATASET = '../questions/dataset-questions.csv'


def test_gdpr():
    df = pd.read_csv(DATASET, encoding="ISO-8859-1")
    df_questions = df.copy().drop(['Unnamed: 0'], axis=1)
    column_names = df_questions.columns.values.tolist()
    logger.info(f"INFO: columns in list {column_names}")
    assert "profile_image" not in column_names



if __name__ == "__main__":
    test_gdpr()
    logger.info(f"INFO: GDPR test run succesfully")
